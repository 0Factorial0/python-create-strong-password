def generate():
    
    import os

    #password list
    password_list = []

    #generate password function
    def generate_password(n):

        import random
        import string
        import time

        #characters
        list0 = ["!","%","?","+","-","/","(",")","="]
        list1 = range(0,10)
        list2 = list(string.ascii_lowercase)
        list3 = list(string.ascii_uppercase)

        #random count of lists list
        list4 = []

        #generate random count of lists
        for x in range(1,5):
            list4.append(random.randint(3,8))

        #password string
        password = ""
        z = 0

        #generate from special characters and add to password string
        for y0 in range(1,list4[z]):
            password = password + str(list0[random.randint(0,8)])

        z += 1

        #generate from number characters and add to password string
        for y1 in range(1,list4[z]):
            password = password + str(list1[random.randint(0,9)])

        z += 1

        #generate from lowercase alphabet characters and add to password string
        for y2 in range(1,list4[z]):
            password = password + str(list2[random.randint(0,25)])

        z += 1

        #generate from uppercase alphabet characters and add to password string
        for y3 in range(1,list4[z]):
            password = password + str(list3[random.randint(0,25)])

        z += 1

        #password string list
        password0 = []
        for x in range(0,len(password)-1):
            password0.append(password[x])
        
        #shuffle the list
        random.shuffle(password0)
        
        #shuffled password string
        password1 = ""
        
        #combine characters in the string
        for x in range(0,len(password0)-1):
            password1 = password1 + password0[x]

        #print out generated passwords
        print("---------------------")
        print("[{0}] = {1}".format(n+1, password1))
        #add password to list
        password_list.append(password1)
        
    #generate x times random number
    import random
    x = random.randint(1, 50)
    for n in range(0, x):
        import time
        time.sleep(0.04)
        generate_password(n)
        
    print("---------------------")
        
    #write passwords in document
    file = open("random_strong_passwords.txt", "a")
    for password in password_list:
        file.write("%s\n" % password)
    file.close()

generate()
